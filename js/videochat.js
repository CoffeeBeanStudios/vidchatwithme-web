$(document).ready(function(){
					
	// Set up my background image size for the full screen background
	var FullscreenrOptions = {  width: 1600, height: 1067, bgID: '#bgimg' };
	// This will activate the full screen background!
	jQuery.fn.fullscreenr(FullscreenrOptions);
	
	// create the chat server
	var chatServer = new ChatServer();

	//tie some events to my buttons
	var createBtn = $("#createBtn");
	var joinBtn = $("#joinBtn");	
	var disconnectBtn = $("#disconnectBtn");
	var emailBtn = $("#emailBtn");
	
	// hide my non visible screens
	$("#secondScreen").hide();
	$("#thirdScreen").hide();
	$("#emailScreen").hide();
	
	//create new meeting blick
	createBtn.bind('click', function(){
		console.log("create clicked");
		
		chatServer.createNew();			
	});
	
	// join meeting click
	joinBtn.bind('click', function(){
		console.log("join clicked");
		
		var key = $("#joinBox").val();
		
		if (key!=""){
			chatServer.joinExisting(key);
		}
	});
	
	emailBtn.bind('click', function(){
		$("#emailResult").text("");
		$("#emailScreen").dialog();				
	});
	
	$("#sendEmailBtn").bind('click', function(){
		console.log('clicked');
		$("#emailResult").text("");
		
		var meetingCode = $("#meetingCode").val();
		var recipient = $("#emailBox").val();
		var subject = "vidChatWith.me Invitation";
		var body = '<body><p>You have been invited to join a video chat meeting!</p><br/>' +
					'<p><a href="http://www.vidChatWith.me/?meeting='+meetingCode+'">Click here to join this meeting from your PC or Mac</a></p></br>' +
					'<p><a href="vidchatwithme://?meeting='+meetingCode+'">Click here to join this meeting from your iPhone or iPad</a></p></br></body>';
		console.log("Body: "  + body);
		
						
		// post to the sending application, then get the results back in JSON
		$.post("sendmail.php", { 
								recipient: recipient, 
								subject: subject,
								body: body},
							function(data){
									console.log("In here: " + data.status);
									if (data.status == "Success"){
										$("#emailResult").text("E-mail sent successfully");
									}
									else{
										$("#emailResult").text("There was an error sending your e-mail...");
									}
								}, "json");
			
		
		
	});
	
	$("#cancelEmailBtn").bind('click', function(){
		$("#emailScreen").dialog('close');
	});
	
	disconnectBtn.bind('click', function(){
		
		// not the fanciest way, but it'll work
		window.location = "http://www.vidchatwith.me"
	});
	
	//check for auto-connect parameter
	var meetingparam = getUrlVars()["meeting"];
	if(meetingparam != ""){
		$("#joinBox").val(meetingparam);
		chatServer.joinExisting(meetingparam);
	}

});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


function ChatServer(){
    "use strict";
	
	var self = this || {};
	
	//get some links to my objects
	var loginDiv = $("#createDiv");
	var joinDiv = $("#joinDiv");
	var status = $("#status");	

    // tracks my state
	var currState = 'CONNECTING';
	self.connected = false;
	var sessionId = "";
	var pubtoken = "";
	var subtoken = "";
	var role = "";
	var in_session = false;
		
	//action callbacks
	this.joinSessionCallback = false;
	this.loginCallback = false;
	this.transferCallback = false;
	
	// if user is running mozilla then use it's built-in WebSocket
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    // if browser doesn't support WebSocket, just show some notification and exit
    if (!window.WebSocket) {
		console.log("Browser does not support WebSockets");
		$("#connectingDialog").innerHtml = 'Sorry, but your browser is too old for this!';
        return;
    }

	//show my connecting dialog and init the connection
	$("#connectingDialog").dialog();	
	var connection = new WebSocket('ws://claude.nodester.com:80');
	
	//sends a message to the server
	self.send = function(msg){
		var json = JSON.stringify(msg);
		console.log(msg);
		$("#joinMessage").text("");
					   
        if (!json) {
            return;
        }		
			
		connection.send(json);
	}

	self.createNew = function(){
		if (!self.connected){return false;}
		
		var msg = {type: "new_session"};
		self.send(msg);
	}	
	
	self.joinExisting = function(subkey){
		if (!self.connected){return false;}
		
		$("#joinMessage").text("");
		
		var msg = {type: "join_session",
				   key: subkey
				  }
		self.send(msg);
		
	}
	
	// publishes local webcam
	function publishOpenTokSession(sessionId, token, user){
		//add the opentok session		
		var apiKey = '14383161'; 
		console.log("My session: " + sessionId);        
			 
		TB.setLogLevel(TB.DEBUG);     
		 
		var session = TB.initSession(sessionId);      
		session.addEventListener('sessionConnected', sessionConnectedHandler);
		session.addEventListener('streamCreated', streamCreatedHandler);   
		session.connect(apiKey, token);
		 
		var publisher;
		 
		function sessionConnectedHandler(event) {
			// Publish my webcam stream and put it in a div
			$("#myImage").hide();
			var props = {width: 400, height:300, name:"Stream"};
			publisher = session.publish("mySession", props);
		}
		
		function streamCreatedHandler(event) {
		}
	
	}
	
	// subscribes to partners stream
	function subscribeOpenTokSession(sessionId, token, user){

		var apiKey = '14383161';        
			
		// create an opentok session
		var session = TB.initSession(sessionId);      
		session.addEventListener('sessionConnected', sessionConnectedHandler);
		session.addEventListener('streamCreated', streamCreatedHandler);   
		session.connect(apiKey, token);
		 
		var subscriber;
		 
		function sessionConnectedHandler(event) {
			// connect to the users stream if it exists
			if (event.streams.length > 0){
				var stream = event.streams[0]; //grab the first stream	
				$("#partnerImage").hide();				
				subscriber = session.subscribe(stream, "partnerSession", {width:400, height:300});
			}
		}
		
		 function streamCreatedHandler(event) {
			// Subscribe to any new streams that are created
			subscribeToStreams(event.streams);
		}
		
		
		function subscribeToStreams(streams) {
			$("#partnerImage").hide();	
			
			//reverse my iterations so i connect to the latest stream
			for (var i = streams.length-1; i >= 0 ; i--){
				if (streams[i].connection.connectionId == session.connection.connectionId){
					continue;
				}

				//subscribe to the first session that isn't ours
				subscriber = session.subscribe(streams[i], "partnerSession", {width:400, height:300});
				return;
			}
		}
	}

	// this fires when the connection is established
    connection.onopen = function () {
		self.connected = true;
		$("#connectingDialog").dialog('close');
    };

    connection.onerror = function (error) {
		$("#connectingDialog").innerHTML = "There was an error with the server... try again!";
		$("#connectingDialog").dialog();
    };

    // most important part - incoming messages
    connection.onmessage = function (message) {
		// parse the json message
        try {
            var json = JSON.parse(message.data);
        } catch (e) {
            console.log('This doesn\'t look like a valid JSON: ', message.data);
            return;
        }

		//handle each message
        if (json.type === 'connected') { // first response from the server with user's color
            //myColor = json.data;
			$("#connectingDialog").dialog('close');
			currState='GET_USERNAME';
		}
		else if (json.type == 'session_result'){
			$("#meetingCode").val(json.key);
			
			sessionId = json.sessionid;
			pubtoken = json.pubtoken;
			subtoken = json.subtoken;
			role = json.role;
			
			//if no one ELSE is in the session yet, we'll just wait for them
			if (json.members == 1){
				$("#secondScreen").show();
				$("#firstScreen").hide();
			}
			// if we have people in there already, just join up
			else{
				publishOpenTokSession(sessionId, pubtoken);
				subscribeOpenTokSession(sessionId, subtoken);
				in_session = true;
				
				$("#secondScreen").hide();
				$("#firstScreen").hide();
				$("#thirdScreen").show();
			}
		}
		else if (json.type == 'join_result'){
			$("#joinMessage").text(json.message);
		}
		else if (json.type == 'new_user'){
			if (!in_session){
				publishOpenTokSession(sessionId, pubtoken);
				subscribeOpenTokSession(sessionId, subtoken);
				in_session = true;
				
				$("#secondScreen").hide();
				$("#firstScreen").hide();
				$("#thirdScreen").show();
			}
		}
    };
};